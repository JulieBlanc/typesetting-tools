# typesetting-tools

A collection of scripts and tools for typesetting

**regex-typo.js**

- correction orthotypo pour le français: oe → œ, XIVème → XVIe, ...
- guillements française correctes, vraie apostrophe et poins de suspensions
- ajout d'espaces fines insécables devant les signes de ponctuation
- ajout d'espaces insécables entre les noms/prénoms, les siècles, devant les nombres (figures, tablers, chapitres...), après certains les mots à une lettre et certains mots à deux lettres
- Ajout des balises d'exposant pour les siècles et certaines abréviations (numéro)
- Ajout de span pour éviter les césures dans les mots à majuscules